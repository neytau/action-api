import hapi from 'hapi'
import Event from './events/events'
import { hapiConf } from './conf/serverConf'

const server = new hapi.Server()
server.connection(hapiConf)

Event(server)

server.start(err => {
    if (err) throw err
    console.log('Server running at : ', server.info.uri)
})