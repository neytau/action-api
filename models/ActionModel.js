export default class {
    constructor(obj = {}) {
        this.action = obj.action,
        this.post = obj.post,
        this.data = obj.data,
        this.timestamp = obj.timestamp
    }
}