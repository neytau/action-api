[![Build status](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://gitlab.com/gitlab-org/gitlab-ce/commits/master)

# Action-API

Api qui gère les actions des utlisateurs.

# Development setup

Renseigner les variables d'environnement.

> Exemple .env: 
```
PORT=
HOST=
REDIS_PORT=
REDIS_HOST=
REDIS_PASS=
RABBITMQ_HOST=
``` 

## Getting Started

Vous pouvez lancer cette commande dans votre terminal : 

```
npm start
```

