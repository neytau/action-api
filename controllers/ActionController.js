import Action from '../models/ActionModel'
import { client } from '../conf/redisConf'
import { queue } from '../conf/rabbitmqConf'

const addEventToQueue = (mess) => {
    queue.then(con => con.createChannel())
    .then(ch => {
        const q = 'event'
        return ch.assertQueue(q).then(ok => {
            return ch.sendToQueue(q, new Buffer(mess))
        })
    }).catch(console.warn)
}

exports.postAction = (msg) => {
    const action = new Action(msg)
    const key = `${action.post}:${action.action}`
    client.multi()
    .LPUSH(key, action.timestamp)
    .EXPIRE(key, 600)
    .exec((err, rep) => {
        if (err) throw err
        console.log(err, rep)
        addEventToQueue(action.post)        
    })
}