import redis from 'redis'
require('dotenv').config()

const REDIS_PORT = process.env.REDIS_PORT 
const REDIS_HOST = process.env.REDIS_HOST 
const REDIS_PASS = process.env.REDIS_PASS

const redisCon = {
    host: REDIS_HOST,
    port: REDIS_PORT,
    password: REDIS_PASS
}

const client = redis.createClient(redisCon)

export { client }