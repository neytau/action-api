require('dotenv').config()

const PORT = process.env.PORT 
const HOST = process.env.HOST 

const hapiConf = {
    port : PORT,
    host : HOST,
    routes: { cors: true }
} 

export { hapiConf }