require('dotenv').config()
import amqp from 'amqplib'

const QUEUE_URL = `amqp://${process.env.RABBITMQ_HOST || 'localhost:5672'}`
const queue = amqp.connect(QUEUE_URL)

export { queue }