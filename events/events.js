import ActionController from '../controllers/ActionController'
import { queue } from '../conf/rabbitmqConf'

module.exports = server => {

    queue.then(con => con.createChannel())
    .then(ch => {
        const q = 'clicked'
        return ch.assertQueue(q).then(ok => {
            return ch.consume(q, msg => {
                if (msg !== null) {
                    ActionController.postAction(JSON.parse(msg.content.toString()))
                    ch.ack(msg)
                }
            })
        })
    }).catch(console.warn)

    queue.then(con => con.createChannel())
    .then(ch => {
        const q = 'mouved'
        return ch.assertQueue(q).then(ok => {
            return ch.consume(q, msg => {
                if (msg !== null) {
                    ActionController.postAction(JSON.parse(msg.content.toString()))
                    ch.ack(msg)
                }
            })
        })
    }).catch(console.warn)
}